// JSON array for tax rates
var taxBrackets = [
  {
    baseWage : 18200,
    loading : 0.19,
    additional : 0 
  },
  {
    baseWage : 37000,
    loading : 0.325,
    additional : 3572
  },
  {
    baseWage : 80000,
    loading : 0.37,
    additional : 17547
  },
  {
    baseWage : 180000,
    loading : 0.45,
    additional : 54547
  },
]

// Set up app
var Payslip = React.createClass({
  getInitialState: function() {
    return {
      payslipTable: null
    }
  },

  // Render output
  render: function() {
    var payslipTable
    if (this.state.payslipTable !== null) {
      
      var data = this.state.payslipTable

      payslipTable = <div>
      <h2>Payslip for {data.firstName} {data.lastName}</h2>
      <div className="table-responsive">
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Pay period</th>
            <th>Gross income</th>
            <th>Income tax</th>
            <th>Net income</th>
            <th>Super</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{data.startDate} to {data.endDate}</td>
            <td>{this.formatCurrency(data.grossSalary)}</td>
            <td>{this.formatCurrency(data.incomeTax)}</td>
            <td>{this.formatCurrency(data.grossSalary - data.incomeTax)}</td>
            <td>{this.formatCurrency(data.super)}</td>
          </tr>
        </tbody>
      </table>
      </div>
      </div>
    }

    return  <div className="row">
      <div className="col-md-6">
        <div className="panel panel-default">
          <div className="panel-heading clearfix">
            <h3 className="panel-title pull-left">Employee Details</h3>
          </div>
          <div className="panel-body">
            <PayslipForm ref="payslipForm" />
          </div>
          <div className="panel-footer">
            <button type="button" className="btn btn-primary btn-block" onClick={this.handleSubmit}>Submit</button>
          </div>
        </div>
      </div>
      <div className="col-md-6">
          {payslipTable}
      </div>
      </div>

  }, 

  handleSubmit: function() {
    if (this.refs.payslipForm.isValid()) {
      this.setState({payslipTable: this.refs.payslipForm.getFormData()})
    }
  },

  //Set currency to 2 decimals with commas. Add $.
  formatCurrency: function(n) {
    return "$"+n.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }

})


var PayslipForm = React.createClass({

  getInitialState: function() {
    return {errors: {}}
  },

  // Set up some validation rules
  isValid: function() {
    var fields = ['firstName', 'lastName', 'salary', 'superRate', 'startDate']

    var errors = {}
    fields.forEach(function(field) {
      var value = this.refs[field].getDOMNode().value
      if (!value) {
        errors[field] = 'This field is required'
      }
      if (field == "startDate" && !moment(value, 'DD-MM-YYYY').isValid()) {
        errors[field] = 'Invalid Date'
      }
      if (field == "superRate" && (value < 0 || value > 50)) {
        errors[field] = 'Super rate must be between 0 and 50'
      }
    }.bind(this))

    this.setState({errors: errors})

    var isValid = true
    for (var error in errors) {
      isValid = false
      break
    }
    return isValid
  },

  // Set up some validation rules
  getFormData: function() {

    var firstName = this.refs.firstName.getDOMNode().value,
      lastName = this.refs.lastName.getDOMNode().value,
      salary = this.refs.salary.getDOMNode().value,
      superRate = this.refs.superRate.getDOMNode().value,
      startDate = this.refs.startDate.getDOMNode().value,
      grossSalary = Math.round(salary / 12)

    var data = {
      firstName: firstName,
      lastName: lastName,
      startDate: startDate,
      endDate: this.renderEndDate(startDate),
      grossSalary: grossSalary,
      incomeTax: this.getIncomeTax(salary),
      super: grossSalary * (superRate/100)
    }

    return data
  },

  render: function() {
    return <div className="form-horizontal">
      {this.renderInput('firstName', 'First name', 'text', 'First name')}
      {this.renderInput('lastName', 'Last name', 'text', 'Last name')}
      {this.renderInput('salary', 'Annual salary', 'number', 'Annual salary')}
      {this.renderInput('superRate', 'Super rate (%)', 'number', 'Super rate')}
      {this.renderInput('startDate', 'Start date', 'text', 'DD/MM/YYYY')}
    </div>
  },

  // Re-usable function to render inputs
  renderInput: function(id, label, type, placeholder) {
    return this.renderField(id, label,
      <input type={type} className="form-control" id={id} ref={id} placeholder={placeholder} />
    )
  },

  // Render fields with bootstrap classes, check validation
  renderField: function(id, label, field) {
    var error = "",
        fieldClass = "form-group"

    if (this.state.errors.hasOwnProperty(id)) {
      error = this.state.errors[id]
      fieldClass = "form-group has-error"
    }

    return <div className={fieldClass}>
      <label htmlFor={id} className="col-sm-4 control-label">{label}</label>
      <div className="col-sm-6">
        {field}
        <span>{error}</span>
      </div>
    </div>
  },

  //Calculate end date. Add one month minus a day.
  renderEndDate : function(date) {
    var endPeriod = moment(date, "DD-MM-YYYY").add(1, 'month').subtract(1, 'day').format("DD/MM/YYYY");
    return endPeriod
  },

  //Calculate income tax based on salary.
  getIncomeTax : function(salary) {

    var tax = 0

    var calculateTax = function(bracket) {
      return Math.round((bracket.additional + (salary - bracket.baseWage)*bracket.loading) / 12)
    }

    switch(true) {
      case (salary <= taxBrackets[0].baseWage):
          tax = 0
          break;
      case (salary > taxBrackets[0].baseWage && salary <= taxBrackets[1].baseWage):
          tax = calculateTax(taxBrackets[0])
          break;
      case (salary > taxBrackets[1].baseWage && salary <= taxBrackets[2].baseWage):
          tax = calculateTax(taxBrackets[1])
          break;
      case (salary > taxBrackets[2].baseWage && salary <= taxBrackets[3].baseWage):
          tax = calculateTax(taxBrackets[2])
          break;
      case (salary > taxBrackets[3].baseWage):
          tax = calculateTax(taxBrackets[3])
          break;
    }

    return tax
  }

})

//Render to DOM
React.render(<Payslip />, document.getElementById('employee-form'))
